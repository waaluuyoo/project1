<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Route::get('/master',function(){
    return view('layout.master');
});

Route::group(['middleware' => ['auth']], function () {
    //
    //dashboard
    Route::get('/dashboard','DashboardController@index');//untuk tampil data

    //crud area
    Route::get('/area/create','AreaController@create');//untuk form
    Route::post('/area','AreaController@store');//untuk save

    Route::get('/area','AreaController@index');//untuk tampil data
    Route::get('/area/{area_id}','AreaController@show');//route detail

    Route::get('/area/{area_id}/edit','AreaController@edit');//route untuk arah ke form edit
    Route::put('/area/{area_id}','AreaController@update');

    Route::delete('/area/{area_id}','AreaController@destroy');//route untuk delete

    //crud hub
    Route::get('/hub/create','HubController@create');//untuk form
    Route::post('/hub','HubController@store');//untuk save

    Route::get('/hub','HubController@index');//untuk tampil data
    Route::get('/hub/{hub_id}','HubController@show');//route detail

    Route::get('/hub/{hub_id}/edit','HubController@edit');//route untuk arah ke form edit
    Route::put('/hub/{hub_id}','HubController@update');

    Route::delete('/hub/{hub_id}','HubController@destroy');//route untuk delete

    //crud customer
    Route::get('/customer/create','CustomerController@create');//untuk form
    Route::post('/customer','CustomerController@store');//untuk save

    Route::get('/customer','CustomerController@index');//untuk tampil data
    Route::get('/customer/{customer_id}','CustomerController@show');//route detail

    Route::get('/customer/{customer_id}/edit','CustomerController@edit');//route untuk arah ke form edit
    Route::put('/customer/{customer_id}','CustomerController@update');

    Route::delete('/customer/{customer_id}','CustomerController@destroy');//route untuk delete

    //crud tiket
    Route::get('/tiket/create','TiketController@create');//untuk form
    Route::post('/tiket','TiketController@store');//untuk save

    Route::get('/tiket','TiketController@index');//untuk tampil data
    Route::get('/tiket/{tiket_id}','TiketController@show');//route detail

    Route::get('/tiket/{tiket_id}/edit','TiketController@edit');//route untuk arah ke form edit
    Route::put('/tiket/{tiket_id}','TiketController@update');

    Route::delete('/tiket/{tiket_id}','TiketController@destroy');//route untuk delete

    //status tiket open
    Route::get('/status_tiket_open','StatustiketopenController@index');//untuk tampil data
    Route::get('/status_tiket_open/{tiket_id}/edit','StatustiketopenController@edit');//route untuk arah ke form edit
    Route::put('/status_tiket_open/{tiket_id}','StatustiketopenController@update');

    //status tiket close
    Route::get('/status_tiket_close','StatustiketcloseController@index');//untuk tampil data
    Route::get('/status_tiket_close/{tiket_id}/edit','StatustiketcloseController@edit');//route untuk arah ke form edit
    Route::put('/status_tiket_close/{tiket_id}','StatustiketcloseController@update');

    //profile
    Route::resource('profile', 'ProfileController')->only([
        'index', 'update'
    ]);
});





Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
