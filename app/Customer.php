<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //
    protected $table = "customer";
    protected $fillable = ["nm_customer", "no_telp","jenis_koneksi_customer","harga_sewa","alamat_customer","hub_id"];
}
