<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status_tiket_close extends Model
{
    //
    protected $table = "tiket";
    protected $fillable = ["customer_id", "keluhan"];
}
